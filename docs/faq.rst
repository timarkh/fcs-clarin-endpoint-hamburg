Short FAQ
=========

Here are some short answers to common questions.

| **Q**: *Is the platform open source and free to use for any purpose?*
| **A**: Yes.

| **Q**: *How do I set up an endpoint for a corpus?*
| **A**: In a nutshell: You fork or copy the repository, clone it on a server, :doc:`configure </configuration>` it, and run it as a web application. Then you configure a CLARIN aggregator, so that it knows where to find your endpoint.

| **Q**: *What kind of requests does an endpoint understand?*
| **A**: GET requests with several parameters. Two operations are available: ``explain`` provides basic info about a corpus, ``searchRetrieve`` returns search results from a corpus. A CQL-like language is used for queries. You will find more details in the specifications_.

| **Q**: *What does an endpoint return?*
| **A**: It returns XML, which contains search results, corpus info and/or error messages. You will find more details in the specifications_.

| **Q**: *How fast is this endpoint?*
| **A**: The endpoint itself only performs the translation, which is very fast. But it sends requests to corpora and must wait for a reply, which can take a long time.

| **Q**: *Can I use one endpoint for multiple corpora?*
| **A**: Yes. You create one configuration file per corpus. Each corpus gets an ID. Queries to different corpora must be sent to different URLs, which include these IDs.

| **Q**: *Does the endpoint understand all of the FCS query language?*
| **A**: It can parse any valid query. However, certain corpus platforms cannot process certain subsets of the query language. If a query is too complex for a corpus, you will get an appropriate diagnostic (i.e. error message) in XML.

| **Q**: *What about data protection?*
| **A**: The endpoint does not store any information on the server and does not place any cookies on the client's machine.

.. _specifications: https://office.clarin.eu/v/CE-2017-1046-FCS-Specification-v89.pdf
