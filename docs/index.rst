.. fcs-clarin-endpoint-hamburg documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FCS Clarin endpoint
===================

Introduction
------------

This is an **endpoint for Federated Content Search** (FCS).

There are many linguistic corpora online. They are available under different platforms and use a variety of query languages. FCS_ is a mechanism that allows you to search in multiple corpora at once, using simple text queries or a CQL-like language. This way, you can discover or compare corpora that can be useful for your research, after which you can proceed to them. This is done through the Aggregator_.

An *endpoint* is a piece of software that serves as an intermediary between FCS and individual corpora. It translates the FCS requests into corpus-specific query languages, waits for the results, and then renders them in an XML format required by the FCS.

Different corpus platforms or online databases require different endpoints. This endpoint works with the following platforms or resources:

* ANNIS_
* Tsakorpus_
* Database of the `Formulae-Litterae-Chartae project`_

.. _FCS: https://www.clarin.eu/content/federated-content-search-clarin-fcs-technical-details
.. _Aggregator: https://contentsearch.clarin.eu/
.. _ANNIS: https://corpus-tools.org/annis/
.. _Tsakorpus: https://github.com/timarkh/tsakorpus
.. _Formulae-Litterae-Chartae project: https://werkstatt.formulae.uni-hamburg.de/collections/formulae_collection

See :doc:`FAQ </faq>` for a short list of commonly asked questions. If you want to learn how to set up an endpoint, please go to :doc:`overview`.

Requirements
------------

This software was tested on Ubuntu and Windows. Its dependencies are the following:

* python >= 3.8
* python modules: ``fastapi``, ``uvicorn``, ``lxml``, ``Jinja2`` (you can use ``requirements.txt``)


License
-------

The software is distributed under CC BY license.


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   faq
   overview
   configuration


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
