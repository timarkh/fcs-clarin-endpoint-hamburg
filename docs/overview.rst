Getting started
===============

Installation
------------

This endpoint does not require installation. All you have to do is make sure all the dependencies are installed and copy the entire contents of the repository to a directory on your computer. This concerns both Linux and Windows. After that, you will need to :doc:`configure </configuration>` the endpoint.

Installing dependencies
~~~~~~~~~~~~~~~~~~~~~~~

Python modules can be installed with the help of the ``requirements.txt`` file in the root folder::

    pip3 install -r requirements.txt

Running the endpoint
--------------------

You can use the endpoint either locally (for testing pruposes) or as a web service available from outside. In the first case, it is sufficient to run ``main.py`` as a Python file. This will start a FastAPI web server. After this, the endpoint for a corpus with the ID ``CORPUS_ID`` will be accessible locally at ``http://127.0.0.1:5000/fcs-endpoint/CORPUS_ID/``.

