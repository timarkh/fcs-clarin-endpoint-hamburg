from fastapi import FastAPI, Request, Query, Response
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from common.query_parser import QueryParser
from common.litterae_query_parser import LitteraeQueryParser
from common.litterae_response_parser import LitteraeResponseParser
from common.tsakorpus_query_parser import TsakorpusQueryParser
from common.tsakorpus_response_parser import TsakorpusResponseParser
from common.annis_query_parser import AnnisQueryParser
from common.annis_response_parser import AnnisResponseParser
from common.enums import *
from common.diagnostics import Diagnostic
from common.config import ResourceConfig, read_configs
from common.views_logic import *
import uvicorn
from a2wsgi import ASGIMiddleware
from datetime import datetime

app = FastAPI()
app.mount('/static', StaticFiles(directory='static'), name='static')
templates = Jinja2Templates(directory='static')

app.qp = QueryParser()

app.qp_litterae = LitteraeQueryParser()
app.rp_litterae = LitteraeResponseParser()
app.qp_tsakorpus = TsakorpusQueryParser()
app.rp_tsakorpus = TsakorpusResponseParser()
app.qp_annis = AnnisQueryParser()
app.rp_annis = AnnisResponseParser()
app.configs = read_configs()
app.logging = True

# The following line is needed in case you want to deploy the endpoint
# under Apache2 with WSGI. Apache's mod_wsgi will import the variable
# named 'application' from this file.
application = ASGIMiddleware(app)


@app.get('/')
def root():
    return {"message": "Hello World"}


@app.get('/fcs-endpoint/{corpusID}')
def endpoint(
        request: Request,
        corpusID: str,
        operation: str = '',
        version: str = '2.0',
        queryType: str = 'cql',
        query: str = '',
        startRecord: str = '1',
        maximumRecords: str = '999999',
        recordPacking: str = '',
        recordXMLEscaping: str = '',
        recordSchema: str = '',
        resultSetTTL: str = '999999',
        stylesheet: str = '',
        extraRequestData: str = '',
        httpAccept: str = 'application/sru+xml',
        xFcsEndpointDescription: str = Query(
            default='',
            alias='x-fcs-endpoint-description'
        ),
        xFcsContext: str = Query(
            default='',
            alias='x-fcs-context'
        ),
        xFcsDataviews: str = Query(
            default='',
            alias='x-fcs-dataviews'
        ),
        xFcsRewritesAllowed: str = Query(
            default='',
            alias='x-fcs-rewrites-allowed'
        )
        ):
    """
    Process incoming HTTP requests. Return an XML response.
    Main parameters are defined here: https://www.loc.gov/standards/sru/sru-1-2.html (SRU 1.2)
    and here http://docs.oasis-open.org/search-ws/searchRetrieve/v1.0/os/part3-sru2.0/searchRetrieve-v1.0-os-part3-sru2.0.html (SRU 2.0).
    Only a part of them is actually taken into account.
    Extra parameters (starting with x-) are defined in the FCS specifications.
    """
    searchOptions = {
        'startRecord': startRecord,
        'maximumRecords': maximumRecords,
        'recordPacking': recordPacking,
        'recordXMLEscaping': recordXMLEscaping,
        'recordSchema': recordSchema,
        'resultSetTTL': resultSetTTL,
        'stylesheet': stylesheet,
        'extraRequestData': extraRequestData,
        'httpAccept': httpAccept,
        'x-fcs-endpoint-description': xFcsEndpointDescription,
        'x-fcs-context': xFcsContext,
        'x-fcs-dataviews': xFcsDataviews,
        'x-fcs-rewrites-allowed': xFcsRewritesAllowed
    }

    if app.logging:
        msg = str(datetime.now()) + '\t' + str(request.query_params) + '\n'
        with open('query_log.txt', 'a', encoding='utf-8') as fLog:
            fLog.write(msg)

    # Validate values of operation, version, queryType and some optional parameters
    operation, version, queryType, searchOptions, failDiagnoctics = initial_validation(operation, version, queryType,
                                                                                       searchOptions, query)

    # Check if the corpus ID is correct
    if corpusID not in app.configs:
        message = 'No corpus with this ID (' + corpusID +') is served by this Endpoint. ' \
                  'Valid corpus IDs are: ' + '; '.join(cID for cID in sorted(app.configs)) + '.'
        failDiagnoctics.append(Diagnostic(DiagnosticTypes.sru, 235, message=message, version=version))  # "Database does not exist"
        config = None
    else:
        config = app.configs[corpusID]

    if len(failDiagnoctics) > 0:
        # This is as far as we can get with bad parameter values
        return process_request(operation, version, queryType, query, searchOptions, config, failDiagnoctics, app, request,
                               templates)

    # Check for common problems with parameter values
    diagnostics = app.qp.validate_query(operation, version, queryType, query,
                                        xFcsEndpointDescription, xFcsContext,
                                        xFcsDataviews, xFcsRewritesAllowed)
    # Now, do the substantial things
    return process_request(operation, version, queryType, query, searchOptions, config, diagnostics, app, request, templates)


if __name__ == '__main__':
    uvicorn.run(
        'main:app',
        host='0.0.0.0',
        port=5000,
        reload=True
    )
