from urllib.parse import quote
import re
import json
import html
from lxml.html import fragment_fromstring
from .enums import *
from .config import ResourceConfig, POSConvertor
from .search_retrieve import Record
from .diagnostics import Diagnostic, DiagnosticTypes


class TsakorpusResponseParser:
    """
    Parses responses from a Tsakorpus instance.
    """
    def __init__(self):
        self.pc = None      # POS convertor, rebuilt with each parse call

    def parse_annotation(self, anno, segID, record):
        """
        Parse HTML annotation for one word taken from a hit.
        Add the data to the layers in the record object.
        """
        print(anno)
        annoTree = fragment_fromstring(anno,
                                       create_parent='div')
        lemmas = set()
        lemmasStr = '_'
        pos = set()
        posStr = '_'
        word = ''
        wordNodes = annoTree.xpath('div[@class="popup_word"]/span[@class="popup_wf"]')
        for node in wordNodes:
            if node.text is not None:
                if len(word) > 0:
                    # This should not happen, but just in case
                    word += '|'
                word += node.text
        lexNodes = annoTree.xpath('div[@class="popup_word"]/'
                                  'div[contains(@class, \'popup_ana\')]/'
                                  'span[@class="popup_lex"]')
        for node in lexNodes:
            if node.text is not None:
                lemmas.add(node.text)
        if len(lemmas) > 0:
            lemmasStr = '|'.join(l for l in sorted(lemmas))
        posNodes = annoTree.xpath('div[@class="popup_word"]/'
                                  'div[contains(@class, \'popup_ana\')]/'
                                  'span[@class="popup_pos"]')
        for node in posNodes:
            if node.text is not None:
                posText = re.sub('&nbsp;|[  \t\ufeff]+', '', node.text)
                posText = self.pc.convert_pos(posText)
                pos.add(posText)
        if len(pos) > 0:
            posStr = '|'.join(p for p in sorted(pos))

        if 'word' not in record.layers:
            record.layers['word'] = []
        record.layers['word'].append({
            'ref': segID,
            'value': word
        })

        if 'pos' not in record.layers:
            record.layers['pos'] = []
        record.layers['pos'].append({
            'ref': segID,
            'value': posStr
        })

        if 'lemma' not in record.layers:
            record.layers['lemma'] = []
        record.layers['lemma'].append({
            'ref': segID,
            'value': lemmasStr
        })

    def parse_span(self, el, record, advancedHits=False):
        """
        Parse one <span> element from the HTML representation
        of one hit returned by a Tsakorpus instance. Add the extracted
        text to the record object.
        """
        if el.tag == 'span' and 'class' in el.attrib and 'sentence_meta' in el.attrib['class']:
            # This is the introductory span that only contains the header
            # (title, author etc.)
            if el.tail is not None:
                record.text += el.tail.strip('\n\t ')
                record.textNoHighlight += el.text.strip('\n\t ')
            return

        if el.text is not None:
            bMatch = False
            if 'class' in el.attrib and re.search('\\bword\\b', el.attrib['class']) is not None:
                if re.search('\\bwmatch\\b', el.attrib['class']) is not None:
                    bMatch = True
                if advancedHits:
                    segID = 's' + str(len(record.segments))
                    segment = {
                        'id': segID,
                        'start': len(record.textNoHighlight) + 1,
                        'end': len(record.textNoHighlight) + len(el.text)
                    }
                    record.segments.append(segment)
                    if 'data-ana' in el.attrib:
                        self.parse_annotation(el.attrib['data-ana'], segID, record)
            record.textNoHighlight += el.text
            if bMatch:
                record.text += '<hits:Hit>' + el.text + '</hits:Hit>'
            else:
                record.text += el.text
        if el.tail is not None:
            record.text += el.tail
            record.textNoHighlight += el.tail

    def parse_context(self, hit, config: ResourceConfig, lang='', advancedHits=False):
        """
        Parse one hit. Return it as a Record object.
        """
        record = Record(advancedHits=advancedHits)
        if len(lang) <= 0:
            lang = config.search_lang_id
        if ('languages' not in hit
                or lang not in hit['languages']
                or 'text' not in hit['languages'][lang]):
            return record
        contentTxt = re.sub('[\r\n\t\ufeff]+', '', hit['languages'][lang]['text'], flags=re.DOTALL)
        # print(contentTxt)
        content = fragment_fromstring(contentTxt,
                                      create_parent='div')
        for el in content:
            self.parse_span(el, record, advancedHits)
        return record

    def parse(self, response, config: ResourceConfig, searchOptions: dict, lang=''):
        """
        Read a dictionary with the first N hits returned by a Tsakorpus
        instance. Return a list of Record objects and the total number of
        records found.
        """
        self.pc = POSConvertor(config)
        diagnostics = []
        advancedHits = False
        dataViewsRequested = {v.strip() for v in searchOptions['x-fcs-dataviews'].split(',') if len(v.strip()) > 0}
        if 'adv' in dataViewsRequested:
            advancedHits = True
        nRecords = 0
        records = []
        if 'n_sentences' in response:
            nRecords = response['n_sentences']
        if searchOptions['startRecord'] > 1 and nRecords < searchOptions['startRecord']:
            # We don't actually care about startRecord, but we should
            # return a fatal diagnostic if it is larger than the number
            # of hits.
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 61))
            return records, nRecords, diagnostics
        if nRecords <= 0 or 'contexts' not in response:
            return records, nRecords, diagnostics
        for context in response['contexts']:
            records.append(self.parse_context(context, config, lang, advancedHits))
        return records, nRecords, diagnostics


if __name__ == '__main__':
    pass

