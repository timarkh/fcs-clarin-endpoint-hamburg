from urllib.parse import quote
import re
import json
import urllib.request
from .query_parser import QueryParser
from .config import ResourceConfig
from .diagnostics import Diagnostic, DiagnosticTypes


class TsakorpusQueryParser(QueryParser):
    """
    Parses search queries for Tsakorpus-based corpora.
    """

    rxTsakorpusBool = re.compile('[()|,]')

    def build_get_string(self, getParams, config: ResourceConfig, searchOptions: dict, withinClause=''):
        """
        Build a GET string (everything after the ?) from a description
        of the GET parameters in the getParams list.
        """
        if len(withinClause) > 0 and withinClause not in ('sentence', 'utterance', 'paragraph'):
            raise Diagnostic(DiagnosticTypes.sru, 48, message='Tsakorpus only supports multi-word search within'
                                                              'one segment that normally equals one sentence / '
                                                              'utterance / turn.')
        termIndexes = self.term_indexes(getParams)
        nWords = len(termIndexes)
        s = 'n_words=' + str(nWords)
        for param in getParams:
            if param[0] in ('word_rel_', 'word_dist_from_', 'word_dist_to_'):
                sfx = '_0'
            else:
                sfx = ''
            s += '&' + param[0] + str(param[1]) + sfx + '=' + quote(str(param[2]))
        for i in termIndexes:
            s += '&lang' + str(i) + '=' + config.search_lang_id
        s += '&page_size=' + str(min(config.max_hits, searchOptions['maximumRecords']))
        s += '&precise=on&sort=random&response_format=json&distance_strict=on'
        return s

    def term_query(self, query: str, config: ResourceConfig):
        """
        Return list of query parameters for one term or sequence of terms.
        """
        if len(query) >= 2 and query.startswith('"') and query.endswith('"'):
            query = query[1:len(query)-1]
        if len(query) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        getParams = []
        iTerm = 0
        for term in query.split(' '):
            if len(term) > 0:
                iTerm += 1
                getParams.append(['wf', iTerm, term])
                if iTerm >= 2:
                    # A maximum of one distance constraint per term
                    getParams.append(['word_rel_', iTerm, iTerm-1])
                    getParams.append(['word_dist_from_', iTerm, '1'])
                    getParams.append(['word_dist_to_', iTerm, '1'])
        return getParams

    def binary_bool(self, strOp: str, operandL, operandR, config):
        if len(operandL) <= 0 or len(operandR) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        termsL = self.term_indexes(operandL)
        operandR = self.shift_term_indexes(operandR, max(termsL))
        termsR = self.term_indexes(operandR)
        if strOp == 'AND':
            if len(termsL) > 1 and len(termsR) > 1:
                message = 'Tsakorpus does not support queries that combine several ' \
                          'multi-word sequences with boolean operators.'
                raise Diagnostic(DiagnosticTypes.sru, 48, message=message)
            return operandL + operandR
        elif strOp == 'OR':
            if len(termsL) > 1 or len(termsR) > 1:
                message = 'Tsakorpus does not support queries that combine several ' \
                          'multi-word sequences with boolean operators.'
                raise Diagnostic(DiagnosticTypes.sru, 48, message=message)
            if operandL[0][0] != 'wf' or operandR[0][0] != 'wf':
                raise Diagnostic(DiagnosticTypes.sru, 47)
            if self.rxTsakorpusBool.search(operandL[0][2]) is not None:
                getParamsNew = [('wf', operandL[0][1], '(' + operandL[0][2] + ')|' + operandR[0][2])]
            else:
                getParamsNew = [('wf', operandL[0][1], operandL[0][2] + '|' + operandR[0][2])]
            return getParamsNew
        raise Diagnostic(DiagnosticTypes.sru, 37, details=strOp)

    def not_bool(self, operand, config):
        # TODO: implement
        raise NotImplementedError()

    def adv_term_query_proper(self, identifier: str, op: str, value: str, flags: str, config: ResourceConfig):
        """
        Return list of query parameters for one term in an advanced query.
        """
        flags = flags.strip('/')
        if len(value) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        if flags not in ('', 'i', 'c'):
            raise Diagnostic(DiagnosticTypes.sru, 48, message='Tsakorpus does not support regex flags.')
        if op not in ('=', '!='):
            raise Diagnostic(DiagnosticTypes.sru, 10,
                             message='In token queries, only = and != are allowed as operators.')
        if op == '!=':
            if identifier != 'pos':
                value = '~' + value
            else:
                value = '~(' + value + ')'
        getParams = []
        if identifier == 'text':
            getParams.append(['wf', 1, value])
        elif identifier == 'lemma':
            getParams.append(['lex', 1, value])
        elif identifier == 'pos':
            if value in config.pos_convert_reverse:
                # UD to corpus-specific POS tags
                value = config.pos_convert_reverse[value]
            getParams.append(['gr', 1, value])
        else:
            raise Diagnostic(DiagnosticTypes.sru, 10,
                              message='The identifier ' + identifier + ' is not supported in Tsakoprus.')
        return getParams

    def adv_quantify_segment(self, getParams, quantifier: str, config: ResourceConfig):
        if len(getParams) != 1 or getParams[0][0] != 'wf' or getParams[0][2] != '.*':
            raise Diagnostic(DiagnosticTypes.sru, 48,
                             message='Token quantifiers are only allowed with empty token queries '
                                     'in Tsakoprus (for setting distance constraints).')
        minDist = 1
        maxDist = 100
        if quantifier == '?':
            maxDist = 2
        elif quantifier == '+':
            minDist = 2
        elif self.rxQuantifierExact.search(quantifier) is not None:
            minDist = maxDist = int(quantifier[1:len(quantifier)-1])
        else:
            m = self.rxQuantifierInterval.search(quantifier)
            if m is None:
                raise Diagnostic(DiagnosticTypes.sru, 10,
                                 message='Something is wrong with a token quantifier.')
            if len(m.group(1)) > 0:
                minDist = int(m.group(1)) + 1
            if len(m.group(2)) > 0:
                maxDist = int(m.group(2)) + 1
        getParams = [
            ['word_rel_', getParams[0][1], getParams[0][1] - 1],
            ['word_dist_from_', getParams[0][1], str(minDist)],
            ['word_dist_to_', getParams[0][1], str(maxDist)]
        ]
        return getParams

    def adv_main_sequence(self, operandL, operandR, config: ResourceConfig):
        # print('SEQUENCE JOIN', str(operandL), str(operandR))
        if len(operandL) <= 0 or len(operandR) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        termsL = self.term_indexes(operandL)
        operandR = self.shift_term_indexes(operandR, max(termsL))
        termsR = self.term_indexes(operandR)
        # Find out if there is already a distance constraint
        wordRelPresent = (any(param[0] == 'word_rel_' for param in operandL)
                          or any(param[0] == 'word_rel_' and param[2] == max(termsL)
                                 for param in operandR))
        if not wordRelPresent:
            wordRelParams = [
                ['word_rel_', min(termsR), max(termsL)],
                ['word_dist_from_', min(termsR), '1'],
                ['word_dist_to_', min(termsR), '1']
            ]
            operandR += wordRelParams
        return operandL + operandR

    def adv_binary_bool(self, strOp: str, operandL, operandR, config: ResourceConfig):
        # Join multiple constraints on one word in an advanced query
        print('ADVANCED INTERNAL BOOL', strOp, str(operandL), str(operandR))
        getParams = []
        if strOp == '&':
            strOp = ','
        paramsR = {paramR[0] for paramR in operandR}
        for paramR in operandR:
            paramExists = False
            for paramL in operandL:
                if paramL[0] == paramR[0]:
                    if strOp == ',' and paramL[0] != 'gr':
                        raise Diagnostic(DiagnosticTypes.sru, 48,
                                         message='Tsakorpus endpoint does not support conjunctions '
                                                 'of multiple constraints for the same layer '
                                                 'within the same word.')
                    paramExists = True
                    getParams.append([paramL[0], paramL[1], '(' + paramL[2] + ')' + strOp + '(' + paramR[2] + ')'])
            if not paramExists:
                getParams.append(paramR[:])
        for paramL in operandL:
            if paramL[0] not in paramsR:
                if strOp == '|':
                    raise Diagnostic(DiagnosticTypes.sru, 48,
                                     message='Tsakorpus does not support disjunctions '
                                             'of constraints for multiple layers '
                                             'within the same word.')
                getParams.append(paramL[:])
        return getParams

    def send_query(self, strGetParams: str, config: ResourceConfig):
        """
        Send the translated query to the Tsakorpus instance. Return JSON results
        returned by the corpus.
        """
        url = config.resource_base_url.strip('/') + '/search_sent?' + strGetParams
        print(url)
        response = urllib.request.urlopen(url)
        data = response.read()
        encoding = response.info().get_content_charset('utf-8')
        responseJSON = json.loads(data.decode(encoding))
        return responseJSON


if __name__ == '__main__':
    pass

