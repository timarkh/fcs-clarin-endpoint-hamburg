from .enums import *
from .diagnostics import Diagnostic
from .config import ResourceConfig
import re
import copy


class QueryParser:
    """
    This class contains commonly used methods for initial parsing of a GET
    query. It does not include platform-specific methods.
    """
    # Regexes for simple search
    rxTermQuery = re.compile('^(?:(?:[^ "]|\\\\")*|"(?:[^"]|\\\\")*")$')
    rxQueryWSpaces = re.compile('[^ \t][ \t]+[^ \t]')

    # Regexes for advanced search
    rxWithinClause = re.compile(' +within +(s|sentence|u|utterance|p|paragraph|'
                                't|turn|text|session) *$')
    rxNonemptyQueryPart = re.compile('[^ \t\r\n]')
    rxSegmentQuery = re.compile('^\\[(.*)\\](\\{[0-9,]+\\}|[?*+]|)$')
    rxAdvTermQuery = re.compile('^ *([a-zA-Z][a-zA-Z0-9\\-]*(?::[a-zA-Z][a-zA-Z0-9\\-]*)?) *'
                                '(!?=) *(["\'].*["\']) *(/[iIcCld])? *$')
    rxQuantifierExact = re.compile('^\\{[1-9][0-9]*\\}$')
    rxQuantifierInterval = re.compile('^\\{(|0|[1-9][0-9]*),(|0|[1-9][0-9]*)\\}$')
    acceptableIdentifiers = {'text', 'lemma', 'pos', 'orth', 'norm', 'phonetic'}

    rxRelOps = re.compile('^(?:word_rel_|word_dist_from_|word_dist_to_)$')

    def __init__(self):
        pass

    @staticmethod
    def find_operator(strQuery, start=0, end=-1):
        """
        Locate the highest NOT, AND or OR operator in a simple query.
        """
        if end == -1:
            end = len(strQuery) - 1
        if strQuery[start:start+3] == 'NOT':
            return start, 'NOT'
        parenthBalance = 0
        inQuotes = False
        for i in range(start, end):
            if inQuotes:
                if strQuery[i] == '"' and (i <= 0 or strQuery[i-1] != '\\'):
                    inQuotes = False
                continue
            if strQuery[i] == '"' and (i <= 0 or strQuery[i-1] != '\\'):
                inQuotes = True
                continue
            if strQuery[i] == '(':
                parenthBalance += 1
            elif strQuery[i] == ')':
                parenthBalance -= 1
            elif parenthBalance == 0:
                if strQuery[i:i+3] == 'AND':
                    return i, 'AND'
                elif strQuery[i:i+2] == 'OR':
                    return i, 'OR'
        return -1, ''

    @staticmethod
    def find_operator_adv(strQuery, start=0, end=-1):
        """
        Locate the highest SEQUENCE (whitespace[s]) or OR (|) operator
        in an advanced query.
        """
        if end == -1:
            end = len(strQuery) - 1
        parenthBalance = 0
        bracketBalance = 0
        curlyBalance = 0
        inQuotes = False
        inSingleQuotes = False
        for i in range(start, end):
            if inQuotes:
                if strQuery[i] == '"' and (i <= 0 or strQuery[i-1] != '\\'):
                    inQuotes = False
                continue
            if strQuery[i] == '"' and (i <= 0 or strQuery[i-1] != '\\'):
                inQuotes = True
                continue
            if inSingleQuotes:
                if strQuery[i] == "'" and (i <= 0 or strQuery[i-1] != '\\'):
                    inSingleQuotes = False
                continue
            if strQuery[i] == "'" and (i <= 0 or strQuery[i-1] != '\\'):
                inSingleQuotes = True
                continue
            if strQuery[i] == '(':
                parenthBalance += 1
            elif strQuery[i] == ')':
                parenthBalance -= 1
            elif strQuery[i] == '[':
                bracketBalance += 1
            elif strQuery[i] == ']':
                bracketBalance -= 1
            elif strQuery[i] == '{':
                curlyBalance += 1
            elif strQuery[i] == '}':
                curlyBalance -= 1
            elif (i > 0 and parenthBalance == 0 and bracketBalance == 0 and curlyBalance == 0
                  and QueryParser.rxNonemptyQueryPart.search(strQuery[:i]) is not None
                  and QueryParser.rxNonemptyQueryPart.search(strQuery[i:]) is not None):
                iCurChar = i
                while iCurChar <= end:
                    if strQuery[iCurChar] != ' ':
                        break
                    iCurChar += 1
                if iCurChar <= end:
                    if strQuery[iCurChar] == '|':
                        return iCurChar, 'OR'
                    elif strQuery[iCurChar] not in '+*?':
                        return iCurChar - 1, 'SEQUENCE'
        return -1, ''

    @staticmethod
    def find_operator_adv_expression(strQuery):
        """
        Locate the highest |, & or ! operator in a segment expression
        in the advanced search.
        """
        start = 0
        end = len(strQuery)
        while start < len(strQuery) and strQuery[start] in ' \t\n':
            start += 1
        while end > 0 and strQuery[end - 1] in ' \t\n':
            end -= 1
        if start >= end:
            return -1, ''
        if strQuery[start] == '!':
            return start, '!'
        parenthBalance = 0
        inQuotes = False
        inSingleQuotes = False
        for i in range(start, end):
            if inQuotes:
                if strQuery[i] == '"' and i > 0 and strQuery[i - 1] != '\\':
                    inQuotes = False
                continue
            if strQuery[i] == '"' and i > 0 and strQuery[i - 1] != '\\':
                inQuotes = True
                continue
            if inSingleQuotes:
                if strQuery[i] == "'" and i > 0 and strQuery[i-1] != '\\':
                    inSingleQuotes = False
                continue
            if strQuery[i] == "'" and i > 0 and strQuery[i-1] != '\\':
                inSingleQuotes = True
                continue
            if strQuery[i] == '(':
                parenthBalance += 1
            elif strQuery[i] == ')':
                parenthBalance -= 1
            elif parenthBalance == 0:
                if strQuery[i] in '|&':
                    return i, strQuery[i]
        return -1, ''

    @staticmethod
    def shift_term_indexes(getParams, shift):
        """
        Increase all search term indexes in the GET parameters
        specified by getParams by shift.
        """
        getParamsShifted = []
        for param in getParams:
            if param[1] <= 0 and all(type(param[i]) == str for i in range(2, len(param))):
                getParamsShifted.append(copy.deepcopy(param))
                continue
            newParam = [param[0]]
            for i in range(1, len(param)):
                # Shift all integer elemnts of the param list
                # int: refers to query word number
                # str: refers to a layer ID, an operator or a distance constraint
                if type(param[i]) is int:
                    newParam.append(param[i] + shift)
                else:
                    newParam.append(param[i])
            getParamsShifted.append(newParam)
        return getParamsShifted

    def term_indexes(self, getParams):
        """
        Find all search term indexes used in the GET parameters
        specified by getParams list. Return list of integers (1-based).
        """
        terms = set()
        for param in getParams:
            if self.rxRelOps.search(param[0]) is not None:
                continue
            elif type(param[2]) is int:
                terms.add(param[2])
            elif type(param[1]) is int:
                terms.add(param[1])
            elif type(param[1]) is list:
                for t in param[1]:
                    terms.add(t)
        if len(terms) <= 0:
            return [0]
        return [t for t in sorted(terms)]

    def rename_params(self, params: dict, config: ResourceConfig):
        """
        If there are corpus-specific names for common tier IDs
        such as "text" or "lemma", make necessary replacements
        in the search parameter list.
        Return replaced list.
        """
        newParams = []
        for param in params:
            paramNew = copy.deepcopy(param)
            if paramNew[0] in config.tier_convert_reverse:
                paramNew[0] = config.tier_convert_reverse[paramNew[0]]
            elif (paramNew[0] == 'tok'
                  and config.platform == 'annis'
                  and 'text' in config.tier_convert_reverse):
                paramNew[0] = config.tier_convert_reverse['text']
            newParams.append(paramNew)
        return newParams

    def build_get_string(self, getParams, config: ResourceConfig, searchOptions: dict, withinClause=''):
        # Abstract function
        raise NotImplementedError()

    def term_query(self, query, config: ResourceConfig):
        # Abstract function
        raise NotImplementedError()

    def binary_bool(self, strOp, operandL, operandR, config: ResourceConfig):
        # Abstract function
        raise NotImplementedError()

    def not_bool(self, operand, config: ResourceConfig):
        # Abstract function
        raise NotImplementedError()

    def adv_term_query_proper(self, identifier: str, op: str, value: str, flags: str, config: ResourceConfig):
        # Abstract function
        raise NotImplementedError()

    def adv_quantify_segment(self, query, quantifier: str, config: ResourceConfig):
        # Abstract function
        raise NotImplementedError()

    def adv_main_sequence(self, resultLeft, resultRight, config: ResourceConfig):
        # Abstract function
        raise NotImplementedError()

    def adv_term_query(self, query, config: ResourceConfig):
        print('ADVANCED TERM QUERY', query)
        if len(query) <= 0:
            query = 'text=".*"'
        m = self.rxAdvTermQuery.search(query)
        if m is None:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        identifier, op, value, flags = m.group(1), m.group(2), m.group(3), m.group(4)
        print('ADVANCED TERM QUERY', identifier, op, value, flags)
        if value[0] != value[-1]:
            raise Diagnostic(DiagnosticTypes.sru, 10)   # Different quotes
        value = value[1:len(value)-1]                   # Remove quotes
        if flags is None:
            flags = ''
        if identifier in ('token', 'word'):
            identifier = 'text'         # Should I do this?
        if identifier not in self.acceptableIdentifiers:
            raise Diagnostic(DiagnosticTypes.sru, 10,
                             message=identifier + ' is not an acceptable identifier in a segment query.')
        return self.adv_term_query_proper(identifier, op, value, flags, config)

    def adv_binary_bool(self, strOp, operandL, operandR, config):
        # Abstract function
        raise NotImplementedError()

    def adv_not_bool(self, operand, config):
        # Abstract function
        raise NotImplementedError()

    def translate_simple(self, query: str, config: ResourceConfig, searchOptions: dict, start=0, end=-1):
        """
        Translate a simple search (CQL) query into a corpus-specific query
        (GET query, JSON Elasticsearch query or whatever).
        If something is wrong with the query, raise a Diagnostic exception.
        This is a top-level platform-independent function. It recursively
        parses the query by locating the hierarchically highest logical operator
        in the current query and then calling a respective lower-level
        function, which may be platform-specific.
        The function is recursive and only looks at the part of the string
        delimited by start and end parameters.
        """
        if end == -1:
            # Top-level call, so return a finalized corpus-specific query
            end = len(query)
            if end == 0:
                raise Diagnostic(DiagnosticTypes.sru, 27)
            if self.rxTermQuery.search(query) is not None:
                return self.build_get_string(self.term_query(query, config), config, searchOptions)
            return self.build_get_string(self.translate_simple(query, config, searchOptions,
                                                               start=start, end=end),
                                         config, searchOptions)
            # if query.count('(') != query.count(')'):
            #     return None
        if len(query) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 27)
        if start >= len(query) - 1 or end <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        while start < len(query) and query[start] in ' \t\n':
            start += 1
        while end > 0 and query[end - 1] in ' \t\n':
            end -= 1
        if start >= end:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        iOpPos, strOp = self.find_operator(query, start, end)
        if iOpPos == -1:
            if query[start] == '(' and query[end - 1] == ')':
                return self.translate_simple(query, config, searchOptions, start=start + 1, end=end - 1)
            else:
                queryPart = query[start:end]
                if ((not queryPart.startswith('"') or not queryPart.endswith('"'))
                        and self.rxQueryWSpaces.search(queryPart.strip('"')) is not None):
                    raise Diagnostic(DiagnosticTypes.sru, 10)
                return self.term_query(queryPart, config)
        if strOp in ('AND', 'OR'):
            resultLeft = self.translate_simple(query, config, searchOptions, start=start, end=iOpPos)
            resultRight = self.translate_simple(query, config, searchOptions, start=iOpPos + len(strOp),
                                                end=end)
            if len(resultLeft) <= 0 or len(resultRight) <= 0:
                raise Diagnostic(DiagnosticTypes.sru, 10)
            return self.binary_bool(strOp, resultLeft, resultRight, config)
        elif strOp == 'NOT':
            resultRight = self.translate_simple(query, config, searchOptions, start=iOpPos + len(strOp),
                                                end=end)
            return self.not_bool(resultRight, config)
        raise Diagnostic(DiagnosticTypes.sru, 10)

    def adv_expression_query(self, query: str, quantifier: str, config: ResourceConfig):
        query = query.strip()
        if len(query) <= 0:
            query = 'text=".*"'
        iOpPos, strOp = self.find_operator_adv_expression(query)
        print('ADVANCED EXPRESSION QUERY', iOpPos, strOp)
        if iOpPos == -1:
            if query[0] == '(' and query[-1] == ')':
                return self.adv_expression_query(query[1:len(query)-1], quantifier, config)
            else:
                if len(quantifier) <= 0:
                    return self.adv_term_query(query, config)
                else:
                    return self.adv_quantify_segment(self.adv_term_query(query, config),
                                                     quantifier, config)
        if strOp in ('&', '|'):
            resultLeft = self.adv_expression_query(query[:iOpPos], '', config)
            resultRight = self.adv_expression_query(query[iOpPos+1:], '', config)
            if len(resultLeft) <= 0 or len(resultRight) <= 0:
                raise Diagnostic(DiagnosticTypes.sru, 10)
            if len(quantifier) <= 0:
                return self.adv_binary_bool(strOp, resultLeft, resultRight, config)
            else:
                return self.adv_quantify_segment(self.adv_binary_bool(strOp, resultLeft, resultRight, config),
                                                 quantifier, config)
        elif strOp == '!':
            resultRight = self.adv_expression_query(query[iOpPos+1:], '', config)
            if len(quantifier) <= 0:
                return self.not_bool(resultRight, config)
            else:
                return self.adv_quantify_segment(self.not_bool(resultRight, config),
                                                 quantifier, config)
        raise Diagnostic(DiagnosticTypes.sru, 10)

    def adv_segment_query(self, query: str, config: ResourceConfig):
        print('ADVANCED SEGMENT QUERY', query)
        m = self.rxSegmentQuery.search(query)
        if m is None:
            raise Diagnostic(DiagnosticTypes.sru, 27)
        expression = m.group(1).strip()
        quantifier = m.group(2)
        return self.adv_expression_query(expression, quantifier, config)

    def adv_simple_query(self, query: str, config: ResourceConfig, start=0, end=-1):
        if len(query) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 27)
        if start >= len(query) - 1 or end <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        while start < len(query) and query[start] in ' \t\n':
            start += 1
        while end > 0 and query[end - 1] in ' \t\n':
            end -= 1
        if start >= end:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        if query[start] == '(' and query[end] == ')':
            return self.adv_main_query(query, config, start=start+1, end=end-1)
        if (query[end - 1] != '\\'
                and ((query[start] == '"' and query[end - 1] == '"')
                     or (query[start] == "'" and query[end - 1] == "'"))):
            return self.adv_segment_query('[text=' + query[start:end] + ']', config)
        return self.adv_segment_query(query[start:end], config)

    def adv_main_query(self, query: str, config: ResourceConfig, start=0, end=-1):
        if len(query) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 27)
        if start >= len(query) - 1 or end <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        while start < len(query) and query[start] in ' \t\n':
            start += 1
        while end > 0 and query[end - 1] in ' \t\n':
            end -= 1
        if start >= end:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        iOpPos, strOp = self.find_operator_adv(query, start, end)
        print('ADVANCED QUERY', iOpPos, strOp)
        if iOpPos == -1:
            return self.adv_simple_query(query, config, start=start, end=end)
        resultLeft = self.adv_simple_query(query, config, start=start, end=iOpPos)
        resultRight = self.adv_main_query(query, config, start=iOpPos + 1, end=end)
        if strOp == 'SEQUENCE':
            if len(resultLeft) <= 0 or len(resultRight) <= 0:
                raise Diagnostic(DiagnosticTypes.sru, 10)
            return self.adv_main_sequence(resultLeft, resultRight, config)
        elif strOp == 'OR':
            resultLeft = self.adv_simple_query(query, config, start=start, end=iOpPos)
            resultRight = self.adv_main_query(query, config, start=iOpPos + 1, end=end)
            if len(resultLeft) <= 0 or len(resultRight) <= 0:
                raise Diagnostic(DiagnosticTypes.sru, 10)
            return self.adv_main_or(resultLeft, resultRight, config)
        raise NotImplementedError

    def translate_advanced(self, query: str, config: ResourceConfig, searchOptions: dict):
        """
        Translate an advanced search (FCS-QL) query into a corpus-specific query
        (GET query, JSON Elasticsearch query or whatever).
        If something is wrong with the query, raise a Diagnostic exception.
        This is a top-level platform-independent function. It recursively
        parses the query by locating the hierarchically highest logical operator
        in the current query and then calling a respective lower-level
        function, which may be platform-specific.
        """
        print('ADVANCED QUERY', query)
        withinClause = ''
        end = len(query)
        m = self.rxWithinClause.search(query)
        if m is not None:
            withinClause = m.group(1)
            if withinClause == 's':
                withinClause = 'sentence'
            elif withinClause == 'u':
                withinClause = 'utterance'
            elif withinClause == 'p':
                withinClause = 'paragraph'
            elif withinClause == 't':
                withinClause = 'turn'
            query = self.rxWithinClause.sub('', query)
            end = len(query)
        if end == 0:
            raise Diagnostic(DiagnosticTypes.sru, 27)
        return self.build_get_string(self.adv_main_query(query, config, start=0, end=end), config, searchOptions,
                                     withinClause=withinClause)

    def validate_query(self, operation, version, queryType, query,
                       xFcsEndpointDescription, xFcsContext,
                       xFcsDataviews, xFcsRewritesAllowed):
        """
        Check if the query parameters contain a valid combination of values.
        Return a list of diagnostics describing problems with the query.
        If the query is prima facie valid and can be processed further, an empty
        list will be returned.
        """
        diagnostics = []

        # The scan operation name is reserved, but not described by the specifications
        if operation == Operation.scan:
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 4, details='scan'))

        # Check if additional parameters combine with the operation requested
        # (FCS specifications, 4.1)
        if len(xFcsEndpointDescription) > 0 and operation != Operation.explain:
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 8, details='x-fcs-endpoint-description'))
        if len(xFcsContext) > 0 and operation != Operation.searchRetrieve:
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 8, details='x-fcs-context'))
        if len(xFcsDataviews) > 0 and operation != Operation.searchRetrieve:
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 8, details='x-fcs-dataviews'))
        if len(xFcsRewritesAllowed) > 0 and operation != Operation.searchRetrieve:
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 8, details='x-fcs-rewrites-allowed'))
        if xFcsRewritesAllowed not in ('', 'true'):
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 6,
                                          message='The value of the parameter x-fcs-rewrites-allowed '
                                                  'can only equal "true".',
                                          details=xFcsRewritesAllowed))
        if xFcsEndpointDescription not in ('', 'true'):
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 6,
                                          message='The value of the parameter x-fcs-endpoint-description '
                                                  'can only equal "true".',
                                          details=xFcsEndpointDescription))

        # Check version-specific parameters and values
        if version == SRUVersion.v1_2:
            if queryType == QueryType.fcs:
                diagnostics.append(Diagnostic(DiagnosticTypes.sru, 8, details='queryType'))
        for dv in xFcsDataviews.split(','):
            dv = dv.strip()
            if len(dv) <= 0:
                continue
            if dv != 'hits' and version == SRUVersion.v1_2:
                diagnostics.append(Diagnostic(DiagnosticTypes.fcs, 4, details=dv))
            elif dv not in ('hits', 'adv'):
                # There actually can be other data view IDs, but they are described in a protected Trac instance
                diagnostics.append(Diagnostic(DiagnosticTypes.fcs, 4, details=dv))

        return diagnostics


if __name__ == '__main__':
    pass

