from urllib.parse import quote
import re
import json
import copy
import random
import urllib.request
from .query_parser import QueryParser
from .config import ResourceConfig
from .diagnostics import Diagnostic, DiagnosticTypes


class LitteraeQueryParser(QueryParser):
    """
    Parses search queries for Formulae, Litterae, Chartae.
    """

    def build_get_string(self, getParams, config, searchOptions: dict, withinClause=''):
        """
        Build a GET string (everything after the ?) from a description
        of the GET parameters in the getParams list.
        """
        if len(withinClause) > 0 and withinClause not in ('text', 'session'):
            raise Diagnostic(DiagnosticTypes.sru, 48, message='FLC web interface only supports multi-word search within'
                                                              'a text.')
        termIndexes = self.term_indexes(getParams)
        nWords = len(termIndexes)
        boolOperatorMentioned = False
        s = 'source=advanced&sort=urn&lemma_search=False'
        for param in getParams:
            nSfx = ''
            if param[1] > 0:
                nSfx = str(param[1])
            if param[0] == 'bool_operator':
                if boolOperatorMentioned:
                    continue
                boolOperatorMentioned = True
            s += '&' + param[0] + nSfx + '=' + quote(str(param[2]))
            if param[0] == 'q_':
                s += '&fuzziness_' + nSfx + '=0'
                s += '&slop_' + nSfx + '=0'
                s += '&in_order_' + nSfx + '=False'
                s += '&search_field_' + nSfx + '=text'
                s += '&exclude_q_' + nSfx + '='
        return s

    def term_query(self, query, config):
        """
        Return list of query parameters for one term or sequence of terms.
        """
        if len(query) >= 2 and query.startswith('"') and query.endswith('"'):
            query = query[1:len(query)-1]
        if len(query) <= 0:
            return Diagnostic(DiagnosticTypes.sru, 10)
        getParams = [['q_', 1, query]]
        return getParams

    def binary_bool(self, strOp, operandL, operandR, config):
        if len(operandL) <= 0 or len(operandR) <= 0:
            raise Diagnostic(DiagnosticTypes.sru, 10)
        termsL = self.term_indexes(operandL)
        operandR = self.shift_term_indexes(operandR, max(termsL))
        termsR = self.term_indexes(operandR)
        if strOp == 'AND':
            getParamsNew = operandL + operandR
            if any(p[0] == 'bool_operator' and p[2] == 'should' for p in getParamsNew):
                message = 'FLC web interface does not support queries that ' \
                          'combine AND and OR.'
                raise Diagnostic(DiagnosticTypes.sru, 48, message=message)
            return getParamsNew + [['bool_operator', -1, 'must']]
        elif strOp == 'OR':
            getParamsNew = operandL + operandR
            if any(p[0] == 'bool_operator' and p[2] == 'must' for p in getParamsNew):
                message = 'FLC web interface does not support queries that ' \
                          'combine AND and OR.'
                raise Diagnostic(DiagnosticTypes.sru, 48, message=message)
            return getParamsNew + [['bool_operator', -1, 'should']]
        raise Diagnostic(DiagnosticTypes.sru, 37, details=strOp)

    def not_bool(self, operand, config):
        # TODO: implement
        raise NotImplementedError()

    def adv_quantify_segment(self, getParams, quantifier: str, config: ResourceConfig):
        """
        This function is not used as of now.
        It is only used in an advanced search, which is switched off for FLC
        for now, and implements the only non-trivial advanced capability that
        exists in FLC, namely distance constraints (set with the 'slop_' parameters
        in the API). If advanced search is enabled for FLC at a future point,
        do not forget to take 'slop_' values into account in build_get_string().
        """
        if len(getParams) != 1 or getParams[0][0] != 'q_' or getParams[0][2] != '.*':
            raise Diagnostic(DiagnosticTypes.sru, 48,
                             message='Token quantifiers are only allowed with empty token queries '
                                     'in Litterae (for setting distance constraints).')
        maxDist = 100
        if quantifier == '?':
            maxDist = 2
        elif quantifier == '+':
            raise Diagnostic(DiagnosticTypes.sru, 48,
                             message='Litterae does not accept "+" as a token quantifier.')
        elif self.rxQuantifierExact.search(quantifier) is not None:
            raise Diagnostic(DiagnosticTypes.sru, 48,
                             message='Litterae does not accept single numbers as token quantifiers.')
        else:
            m = self.rxQuantifierInterval.search(quantifier)
            if m is None:
                raise Diagnostic(DiagnosticTypes.sru, 10,
                                 message='Something is wrong with a token quantifier.')
            if len(m.group(1)) > 0 and m.group(1) != '0':
                raise Diagnostic(DiagnosticTypes.sru, 48,
                                 message='Litterae does not accept token quantifiers with'
                                         'positive lower bounds.')
            if len(m.group(2)) > 0:
                maxDist = int(m.group(2))
        getParams = ['slop_', getParams[0][1], str(maxDist)]
        return getParams

    def send_query(self, strGetParams: str, config: ResourceConfig):
        """
        Send the translated query to the Litterae instance. Return JSON results
        returned by the corpus.
        """
        url = config.resource_base_url.strip('/') + '/results?' + strGetParams
        response = urllib.request.urlopen(url)
        data = response.read()
        encoding = response.info().get_content_charset('utf-8')
        responseHTML = data.decode(encoding)
        return responseHTML


if __name__ == '__main__':
    pass

