from enum import Enum

# All enumerators used for query validation are defined here

class CorpPlatform(str, Enum):
    tsakorpus = 'tsakorpus'
    annis = 'annis'
    litterae = 'litterae'


class Operation(str, Enum):
    explain = 'explain'
    searchRetrieve = 'searchRetrieve'
    scan = 'scan'


class SRUVersion(str, Enum):
    v1_2 = '1.2'
    v2_0 = '2.0'


class QueryType(str, Enum):
    # Query language (parameter used since SRU 2.0)
    fcs = 'fcs'
    cql = 'cql'     # Contextual Query Language; default


class DiagnosticTypes(str, Enum):
    # Diagnostic type (defines its namespace)
    sru = 'sru'     # Defined at http://www.loc.gov/standards/sru/diagnostics/diagnosticsList.html
    fcs = 'fcs'     # Defined in the FCS specifications, 4.2


class DataView(str, Enum):
    # Data view (simple hits / multi-layered hits with annotation)
    hits = 'hits'
    adv = 'adv'


if __name__ == '__main__':
    pass

