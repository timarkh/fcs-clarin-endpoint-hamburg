from urllib.parse import quote
import re
import json
import html
from lxml.html import fromstring, tostring
from .enums import *
from .config import ResourceConfig
from .search_retrieve import Record
from .diagnostics import Diagnostic, DiagnosticTypes


class LitteraeResponseParser:
    """
    Parses responses from a Litterae instance.
    """
    rxNHits = re.compile('(?:Suchergebnisse:|Search [rR]esults:)[ \t\r\n]*([0-9]+)')
    rxUselessTags = re.compile('</?(?:p|small)[^\r\n<>]*>')
    rxHitTag = re.compile('(</?)strong>')

    def __init__(self):
        self.pc = None      # POS convertor, rebuilt with each parse call

    def process_hits(self, tableNode, config: ResourceConfig, searchOptions: dict,
                     diagnostics: list[Diagnostic], advancedHits=False):
        """
        Process hits from an HTML node with the results table.
        If anything goes wrong, add Diagnostic objects to diagnostics list.
        Return a list of Record objects.
        Since only the Generic Hits view is available for Litterae,
        advancedHits parameter will not actually be used.
        """
        records = []
        rows = tableNode.xpath('tr')
        iRow = 0
        iRowOffset = 0
        maxHits = min(config.max_hits, searchOptions['maximumRecords'])
        while iRow < len(rows) and iRow - iRowOffset < maxHits:
            row = rows[iRow]
            iRow += 1
            paragraphs = row.xpath('td/p')
            if len(paragraphs) <= 0:
                iRowOffset += 1
                continue
            record = Record(advancedHits=False)
            txtParagraphs = []
            for p in paragraphs:
                txt = tostring(p, encoding='utf-8').decode('utf-8')
                print(txt, type(txt))
                txt = self.rxUselessTags.sub('', txt)
                txt = self.rxHitTag.sub('\\1hits:Hit>', txt)
                txtParagraphs.append(txt.strip())
            record.text = ' &lt;...&gt; '.join(txtParagraphs).strip()
            records.append(record)
        return records

    def parse(self, response, config: ResourceConfig, searchOptions: dict):
        """
        Read HTML response with the first N hits returned by a Litterae
        instance. Return a list of Record objects and the total number of
        records found.
        Since only the Generic Hits view is available for Litterae,
        xFcsDataviews parameter will not actually be used.
        """
        diagnostics = []
        advancedHits = False
        dataViewsRequested = {v.strip() for v in searchOptions['x-fcs-dataviews'].split(',') if len(v.strip()) > 0}
        if 'adv' in dataViewsRequested:
            advancedHits = True
        srcTree = fromstring(response)
        nRecords = 0
        nHitsNode = srcTree.xpath('//article[@class="container-fluid"]/header/h1')
        if len(nHitsNode) > 0 and nHitsNode[0].text is not None:
            m = self.rxNHits.search(nHitsNode[0].text)
            if m is not None:
                nRecords = int(m.group(1))
        resTableNodes = srcTree.xpath('//table[@id="partsSearchResultTable"]/tbody')
        records = []
        if searchOptions['startRecord'] > 1 and nRecords < searchOptions['startRecord']:
            # We don't actually care about startRecord, but we should
            # return a fatal diagnostic if it is larger than the number
            # of hits.
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 61))
            return records, nRecords, diagnostics
        if len(resTableNodes) <= 0:
            nRecords = 0
        else:
            records = self.process_hits(resTableNodes[0], config, searchOptions,
                                        diagnostics, advancedHits=advancedHits)
        if (len(records) < nRecords
                and len(records) < config.max_hits
                and len(records) < searchOptions['maximumRecords']):
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 59,
                                          message='Some results could not be shown due to copyright restrictions.'))
        return records, nRecords, diagnostics


if __name__ == '__main__':
    pass

