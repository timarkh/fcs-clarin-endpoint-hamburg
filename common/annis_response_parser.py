from urllib.parse import quote
import re
import networkx as nx
from lxml.html import fromstring, tostring
from .enums import *
from .config import ResourceConfig
from .search_retrieve import Record
from .diagnostics import Diagnostic, DiagnosticTypes


class AnnisResponseParser:
    """
    Parses responses from an ANNIS instance.
    """

    rxNodeIDPfx = re.compile('^[^/]*::')

    def __init__(self):
        self.pc = None      # POS convertor, rebuilt with each parse call

    def node_anno_value(self, node):
        """
        Return annotation value for a node, represented by
        a node data dictionary.
        """
        try:
            return node['annis::tok']
        except KeyError:
            pass
        return ''

    def node_tier_value(self, node, config):
        """
        Return tier name for a node, represented by
        a node data dictionary.
        """
        for k in node:
            if k.startswith('annis::'):
                continue
            k = self.rxNodeIDPfx.sub('', k)
            if k in config.tier_convert:
                return config.tier_convert[k]
        return None

    def process_token_sequences(self, seqs, hit, nodeData, highlightNodes,
                                config: ResourceConfig, searchOptions: dict,
                                diagnostics: list[Diagnostic], advancedHits=False):
        """
        Extract information from several sequences of token nodes
        and their descendants, which represent one search hit.
        Return a Record object with all the data.
        """
        record = Record(advancedHits=advancedHits)
        # print(highlightNodes)
        for iSeq in range(len(seqs)):
            seq = seqs[iSeq]
            for tokenNodeID in seq:
                node = nodeData[tokenNodeID]
                # print(tokenNodeID, node)
                token = self.node_anno_value(node)
                if advancedHits:
                    segID = 's' + str(len(record.segments))
                    segment = {
                        'id': segID,
                        'start': len(record.textNoHighlight) + 1,
                        'end': len(record.textNoHighlight) + len(token)
                    }
                    record.segments.append(segment)
                    # Now extract data from all relevant tiers.
                    # This is tricky: nodes are linked not only to the
                    # respective annotation nodes, but also to the next
                    # node on the same level. So we use depth-first search
                    # and break once we see another node from a layer
                    # we have already seen.
                    usedLayers = set()
                    # First, the token
                    tierName = self.node_tier_value(node, config)
                    if tierName is not None:
                        if tierName not in record.layers:
                            record.layers[tierName] = []
                        record.layers[tierName].append({
                            'ref': segID,
                            'value': token
                        })

                    # Then all its annotations
                    for e in nx.dfs_edges(hit, tokenNodeID):
                        descendantNode = nodeData[e[1]]
                        annoTierName = self.node_tier_value(descendantNode, config)
                        annoValue = self.node_anno_value(descendantNode)
                        if annoTierName is not None and annoTierName in usedLayers:
                            break
                        usedLayers.add(annoTierName)
                        if annoTierName is not None and len(annoValue) > 0:
                            if annoTierName not in record.layers:
                                record.layers[annoTierName] = []
                            record.layers[annoTierName].append({
                                'ref': segID,
                                'value': annoValue
                            })
                record.textNoHighlight += token + ' '
                if tokenNodeID in highlightNodes:
                    record.text += '<hits:Hit>' + token + '</hits:Hit> '
                else:
                    record.text += token + ' '
            if iSeq < len(seqs) - 1:
                record.textNoHighlight += '... '
                record.text += '... '
        return record

    def process_subgraph(self, hit, highlightNodes,
                         config: ResourceConfig, searchOptions: dict,
                         diagnostics: list[Diagnostic], advancedHits=False):
        """
        Process one hit returned by ANNIS, stored as a networkx graph.
        Return a Record object.
        """
        highlightNodes = [self.rxNodeIDPfx.sub('', nodeID) for nodeID in highlightNodes]
        # An annotated text segment in ANNIS is a subgraph, where some
        # nodes are descendant of another nodes, e.g. tokens are descendants
        # of a text. This is too complicated for an FCS output, where you
        # have to have tokens, each of which can have some annotation at
        # different layers, such as lemma or pos. So we only look at the
        # ANNIS subgraph nodes that are labeled as belonging to tokenLayer,
        # count them as tokens, and look for their annotation in their
        # descendant nodes.
        tokenLayer = 'tok'
        if 'text' in config.tier_convert_reverse:
            tokenLayer = config.tier_convert_reverse['text']

        # ANNIS can find several disjoint segments that belong to the same
        # text, each of which contains at least one search term. If this is
        # the case, there will be several disconnected subgraphs in the response,
        # so there can be multiple roots.
        roots = [n for n, d in hit.in_degree() if d == 0]
        tokenSequences = []
        nodeData = {
            node[0]: node[1] for node in hit.nodes(data=True)
        }
        for root in roots:
            usedNodes = set()
            tokenSequences.append([])
            for e in nx.bfs_edges(hit, root):
                # print(hit.get_edge_data(e[0], e[1]))
                for side in (0, 1):
                    if e[side] in usedNodes:
                        continue
                    usedNodes.add(e[side])
                    node = nodeData[e[side]]
                    for k in node.keys():
                        if not k.startswith('annis::') and self.rxNodeIDPfx.sub('', k) == tokenLayer:
                            tokenSequences[-1].append(e[side])
                        # if k == 'annis::tok':
                        #     tok = node[k]
        record = self.process_token_sequences(tokenSequences, hit, nodeData, highlightNodes,
                                              config, searchOptions,
                                              diagnostics, advancedHits)
        return record

    def process_hits(self, hits, highlightNodes,
                     config: ResourceConfig, searchOptions: dict,
                     diagnostics: list[Diagnostic], advancedHits=False):
        """
        Process hits returned by ANNIS, which take the form of
        graphML strings.
        If anything goes wrong, add Diagnostic objects to diagnostics list.
        Return a list of Record objects.
        """
        records = []
        for iHit in range(len(hits)):
            try:
                hits[iHit] = nx.parse_graphml(hits[iHit].decode('utf-8'))
            except:
                raise Diagnostic(DiagnosticTypes.sru, 1,
                                 details='Could not parse graphML data returned by the ANNIS instance.')

        for iHit in range(len(hits)):
            # for node, data in hit.nodes(data=True):
            #     print(node, data)
            record = self.process_subgraph(hits[iHit], highlightNodes[iHit], config,
                                           searchOptions, diagnostics, advancedHits)
            if record is not None:
                records.append(record)
        return records

    def parse(self, responseData, config: ResourceConfig, searchOptions: dict):
        """
        Read graphML response with the first N hits returned by an ANNIS
        instance. The hits to process are stored in responseData['hits'].
        Return a list of Record objects and the total number of
        records found.
        """
        diagnostics = []
        advancedHits = False
        dataViewsRequested = {v.strip() for v in searchOptions['x-fcs-dataviews'].split(',') if len(v.strip()) > 0}
        if 'adv' in dataViewsRequested:
            advancedHits = True
        nRecords = responseData['n_hits']
        records = []
        if searchOptions['startRecord'] > 1 and nRecords < searchOptions['startRecord']:
            # We don't actually care about startRecord, but we should
            # return a fatal diagnostic if it is larger than the number
            # of hits.
            diagnostics.append(Diagnostic(DiagnosticTypes.sru, 61))
            return records, nRecords, diagnostics
        if len(responseData['hits']) <= 0:
            nRecords = 0
        else:
            try:
                records = self.process_hits(responseData['hits'], responseData['hit_ids'],
                                            config, searchOptions,
                                            diagnostics, advancedHits=advancedHits)
            except Diagnostic as d:
                diagnostics.append(d)
        return records, nRecords, diagnostics


if __name__ == '__main__':
    pass

