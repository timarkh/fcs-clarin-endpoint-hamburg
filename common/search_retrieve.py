from .enums import *
from .diagnostics import Diagnostic
from .config import ResourceConfig


class Record:
    """
    Per FCS specifications, one hit should be encoded as one record,
    even though in SRU, records can contain resources, resources
    can contain resource fragments, and they, in turn, can contain
    multiple hits. Here, each record contains exactly one resource
    with exactly one fragment with exactly one hit.
    """
    def __init__(self, advancedHits: bool = False):
        self.advancedHits = advancedHits
        # For simple search:
        self.text = ''
        self.textNoHighlight = ''   # no <hits:Hit> elements, just text
        # For advanced search:
        self.segments = []
        self.layers = {}            # ID -> content

    def as_dict(self):
        """
        Returns a dictionary for insertion into the XML template.
        """
        # In generic SRU, records, resources and resource fragments
        # are all distinct entities. In FCS, it's the same thing. Still,
        # I model the templates in a generic way, in case that changes
        # in FCS in the future
        record = {
            'resources': [{
                'resource_fragments': [{
                    'dv_hits': [{
                        'text': self.text
                    }],
                    'dv_adv': []
                }]
            }]
        }
        if self.advancedHits:
            record['resources'][0]['resource_fragments'][0]['dv_adv'].append({
                'segments': self.segments,
                'layers': self.layers
            })
        return record


if __name__ == '__main__':
    pass

