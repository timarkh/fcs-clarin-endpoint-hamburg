"""
Contains a class that handles endpoint configuration. Its main
functions are reading the configuration files and restoring the
defaults if some keys are absent there.
"""
from .enums import *
import json
import copy
import re
import os


class ResourceConfig:
    """
    Properties of this class correspond to the keys in a configuration file
    for one of the resources this endpoint communicates with.
    """

    rxExt = re.compile('\\.[^.]*$')

    def __init__(self, fnameConfig=None):
        self.corpus_id = ''
        self.platform = 'tsakorpus'
        self.transport_protocol = 'https'
        self.host = '127.0.0.1'
        self.port = '5000'
        self.url_path = '127.0.0.1'
        self.resource_base_url = 'http://127.0.0.1'
        self.annis_corpus_id = ''     # ANNIS-internal ID of the corpus to search in
        self.annis_context_size = 5   # Context size for hits rendering (ANNIS only)
        self.titles = []
        self.descriptions = []
        self.authors = []
        self.contacts = []
        self.extents = []
        self.history = []
        self.restrictions = []
        self.max_hits = 10
        self.basic_search_capability = True
        self.advanced_search_capability = False
        self.hits_supported = True
        self.adv_supported = False
        self.supported_layers = []
        self.resources = []
        self.search_lang_id = ''
        self.pos_convert = []           # corpus-specific to UD (regexes)
        self.pos_convert_reverse = {}   # UD to corpus-specific
        self.tier_convert = {}          # corpus-specific tier IDs to Advanced view layer labels
        self.tier_convert_reverse = {}  # FCS to corpus-specific tier IDs

        self.query_timeout = 60

        # NB: The following properties are not used right now.
        # They may be used if somebody develops a GUI for editing
        # configuration files in the future.
        self.boolParams = set(k for k in self.__dict__
                              if type(self.__dict__[k]) == bool)
        self.intParams = set(k for k in self.__dict__
                             if type(self.__dict__[k]) == int)
        self.lsParams = set()
        # dictionaries where values are strings
        self.dict_sParams = {'pos_convert_reverse'}
        # dictionaries where values are lists of strings
        self.dict_lsParams = {'pos_convert'}
        # dictionaries where values are dictionaries {k: string}
        self.dict_dParams = set()

        if fnameConfig is not None and os.path.exists(fnameConfig):
            self.load_settings(fnameConfig)


    def add_default_lang(self, elements):
        """
        Add the default "lang"="en" attribute to all elements
        in a list that do not have a "lang" attribute.
        """
        for el in elements:
            if 'lang' not in el:
                el['lang'] = 'en'

    def load_settings(self, fnameConfig):
        """
        Load configuration for one of the resources from a JSON file.
        """
        with open(fnameConfig, 'r', encoding='utf-8') as fConfig:
            config = json.load(fConfig)
        if len(self.corpus_id) <= 0:
            self.corpus_id = re.sub('.*?([^/\\\\]+)\\.[^.]*$', '\\1', fnameConfig)
        for k, v in config.items():
            setattr(self, k, v)
        self.add_default_lang(self.titles)
        self.add_default_lang(self.descriptions)
        self.add_default_lang(self.authors)
        self.add_default_lang(self.contacts)
        self.add_default_lang(self.extents)
        self.add_default_lang(self.history)
        self.add_default_lang(self.restrictions)
        if self.platform == 'tsakorpus' and self.adv_supported:
            if len(self.supported_layers) <= 0:
                self.supported_layers = [
                    {
                        'id': 'word',
                        'result-id': 'word',    # I have no idea what this is
                        'layer_type': 'text'
                    },
                    {
                        'id': 'lemma',
                        'result-id': 'lemma',   # I have no idea what this is
                        'layer_type': 'lemma'
                    },
                    {
                        'id': 'pos',
                        'result-id': 'pos',     # I have no idea what this is
                        'layer_type': 'pos'
                    }
                ]
        for r in self.resources:
            if 'pid' not in r:
                r['pid'] = ''
            if 'titles' not in r:
                r['titles'] = []
            if 'descriptions' not in r:
                r['descriptions'] = []
            if 'languages' not in r:
                r['languages'] = []
            if 'data_views' not in r or len(r['data_views']) <= 0:
                r['data_views'] = 'hits'
            if 'layers' not in r:
                if self.adv_supported and self.platform == 'tsakorpus':
                    r['layers'] = 'word lemma pos'
                else:
                    r['layers'] = ''
            self.add_default_lang(r['titles'])
            self.add_default_lang(r['descriptions'])

    def as_dict(self):
        """
        Return current settings as a dictionary.
        """
        dictSettings = copy.deepcopy(vars(self))
        for k in [_ for _ in dictSettings.keys()]:
            if dictSettings[k] is None:
                dictSettings[k] = ''
        return dictSettings

    def gui_str_to_dict(self, s, value_type='list'):
        """
        Process one input string that describes a dictionary.
        NB: This function is not used right now. It may be used if
        somebody develops a GUI for editing configuration files
        in the future.
        """
        d = {}
        s = s.replace('\r', '').strip()
        s = re.sub('\n\n+', '\n', s, flags=re.DOTALL)
        if value_type == 'dict':
            prevKey = ''
            curData = {}
            for line in s.split('\n'):
                if not line.startswith(' '):
                    curKey = line.strip(': ')
                    if len(prevKey) > 0 and curKey != prevKey:
                        d[prevKey] = curData
                        curData = {}
                    prevKey = curKey
                else:
                    line = line.strip()
                    if ':' not in line:
                        continue
                    k, v = line.split(':')
                    k = k.rstrip()
                    v = v.lstrip()
                    curData[k] = v
            if len(curData) > 0:
                d[prevKey] = curData
        else:
            for line in s.split('\n'):
                line = line.strip()
                if ':' not in line:
                    continue
                k, v = line.split(':')
                k = k.rstrip()
                v = v.lstrip()
                if value_type == 'list':
                    if len(v) <= 0:
                        v = []
                    else:
                        v = [vp.strip() for vp in v.split(',')]
                d[k] = v
        return d

    def processed_gui_config(self, data):
        """
        Turn form data filled by the user in the configuration GUI to
        a dictionary in the correct format.
        NB: This function is not used right now. It may be used if
        somebody develops a GUI for editing configuration files
        in the future.
        """
        dictConfig = {}
        for f in self.boolParams:
            if f in data and len(data[f]) > 0:
                dictConfig[f] = True
            else:
                dictConfig[f] = False
        for f in self.intParams:
            if f in data and len(data[f]) > 0:
                dictConfig[f] = int(data[f])
        for f in self.lsParams:
            if f in data and len(data[f]) > 0:
                dictConfig[f] = [v.strip() for v in data[f].replace('\r', '').strip().split('\n')]
            else:
                dictConfig[f] = []
        for f in self.dict_sParams:
            if f in data and len(data[f]) > 0:
                dictConfig[f] = self.gui_str_to_dict(data[f], value_type='string')
            else:
                dictConfig[f] = {}
        for f in self.dict_lsParams:
            if f in data and len(data[f]) > 0:
                dictConfig[f] = self.gui_str_to_dict(data[f], value_type='list')
            else:
                dictConfig[f] = {}
        for k, v in data.items():
            if '%' in k:
                continue
            if k not in dictConfig:
                dictConfig[k] = v
        return dictConfig

    def save_settings(self, fnameOut, data=None):
        """
        Save current or new configuration as a JSON file (can be used to edit
        configuration files through a web interface).
        NB: This function is not used right now. It may be used if
        somebody develops a GUI for editing configuration files
        in the future.
        """
        if data is None or len(data) <= 0:
            dictConfig = self.as_dict()
        else:
            dictConfig = self.processed_gui_config(data)
        with open(fnameOut, 'w', encoding='utf-8') as fOut:
            json.dump(dictConfig, fOut, sort_keys=True, ensure_ascii=False, indent=2)


class POSConvertor:
    """
    Convert corpus-specific parts of speech / grammar tags to
    UPOS, using regexes correspondences set in the config.
    """
    def __init__(self, config: ResourceConfig):
        self.posConvert = config.pos_convert
        self.posConvertReverse = config.pos_convert_reverse
        self.posTests = [(re.compile(k), v) for k, v in self.posConvert]

    def convert_pos(self, pos):
        """
        Convert corpus-specific POS tags to UPOS, if possible.
        Regexes are sequentially applied to the corpus-specific
        string with POS or more detailed grammatical tags. The
        first regex in the list that matches wins.
        """
        for k, v in self.posTests:
            if k.search(pos) is not None:
                return v
        return pos

    def convert_ud_pos(self, udPos):
        """
        Convert UD POS to a corpus-specific POS tag or string
        with more detailed grammatical tags.
        """
        try:
            return self.posConvertReverse[udPos]
        except KeyError:
            return udPos


def read_configs(configDir='./config'):
    """
    Load all configuration files from the configuration directory
    (one file per resource). Initialize ResourceConfig instances for
    each of them. Return a dictionary where the keys are filenames and
    the values contain respective ResourceConfig objects.
    """
    configs = {}
    for fname in os.listdir(configDir):
        if not fname.lower().endswith('.json'):
            continue
        fnameFull = os.path.join(configDir, fname)
        fnameNoExt = ResourceConfig.rxExt.sub('', fname)
        configs[fnameNoExt] = ResourceConfig(fnameFull)
    return configs
