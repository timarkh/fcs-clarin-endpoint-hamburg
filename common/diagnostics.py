from .enums import *
import jinja2


class Diagnostic(Exception):
    """
    Contains methods for issuing diagnostic messages (fatal or non-fatal)
    as per FCS specifications.
    Read more about SRU diagnostics here: https://www.loc.gov/standards/sru/diagnostics/ .
    SRU diagnostics list is located here: http://www.loc.gov/standards/sru/diagnostics/diagnosticsList.html .
    Additional FCS diagnostics are listed in the FCS specifications.
    """

    fatalFCSDiagnostics = {3, 10, 11}      # FCS specifications, 4.2
    fatalSRUDiagnostics = {1, 4, 5, 6, 8, 10, 27, 37, 47, 48, 61, 71, 235}     # A subset actually used by this endpoint

    stdMessages = {
        (DiagnosticTypes.fcs, 4): 'Requested Data View not valid for this resource.',
        (DiagnosticTypes.sru, 1): 'General system error.',
        (DiagnosticTypes.sru, 4): 'Unsupported operation. Supported operation: explain, searchRetrieve, scan.',
        (DiagnosticTypes.sru, 5): 'Unsupported version. Supported SRU versions: 1.2 and 2.0.',
        (DiagnosticTypes.sru, 8): 'Unsupported parameter.',
        (DiagnosticTypes.sru, 10): 'Something is wrong with the query syntax.',
        (DiagnosticTypes.sru, 27): 'The query should not be empty.',
        (DiagnosticTypes.sru, 37): 'Unsupported boolean operator.',
        (DiagnosticTypes.sru, 61): 'Start record position out of range.'
    }

    def __init__(self, diagType: DiagnosticTypes, diagID: int,
                 details: str = '',
                 message: str = '',
                 version: SRUVersion = SRUVersion.v2_0):
        """
        Initialize a diagnostic with a given numerical ID.
        """
        self.diagType = diagType
        self.diagID = diagID
        self.details = details
        self.message = message
        self.version = version
        if len(self.message) <= 0 and (diagType, diagID) in self.stdMessages:
            self.message = self.stdMessages[(diagType, diagID)]

        self.templateLoader = jinja2.FileSystemLoader(searchpath="./static")
        self.templateEnv = jinja2.Environment(loader=self.templateLoader)

    def is_fatal(self):
        """
        Return True iff the diagnostic with the given ID is fatal,
        i.e. the query should not be processed further.
        """
        if self.diagType == DiagnosticTypes.fcs:
            return self.diagID in self.fatalFCSDiagnostics
        elif self.diagType == DiagnosticTypes.sru:
            return self.diagID in self.fatalSRUDiagnostics
        return True

    def uri(self):
        """
        Return URI representing this diagnostic.
        """
        if self.diagType == DiagnosticTypes.fcs:
            return 'http://clarin.eu/fcs/diagnostic/' + str(self.diagID)
        elif self.diagType == DiagnosticTypes.sru:
            return 'info:srw/diagnostic/1/' + str(self.diagID)
        return ''

    def __str__(self):
        """
        Return the XML version of this diagnostic.
        """
        if self.version == SRUVersion.v1_2:
            templateVersion = 1
        else:
            templateVersion = 2
        template = self.templateEnv.get_template('diagnostic.xml')
        xmlText = template.render(uri=self.uri(),
                                  details=self.details,
                                  message=self.message,
                                  version=templateVersion)
        return xmlText.strip()

    def __repr__(self):
        return str(self)


if __name__ == '__main__':
    # Test
    d = Diagnostic(DiagnosticTypes.fcs, 3)
    d.message = '123'
    print(d)
