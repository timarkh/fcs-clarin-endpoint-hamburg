p. 2: Link 'CLARIN-FCS-DataViews' leads to a CLARIN-internal Trac instance.

p. 3: "The value of the @version attribute MUST be 2." But SRU 1.2 EndpointDescription will have version="1", per example on p. 12. (By the way, some examples are numbered and some are not.)

p. 4: Link 'section "Layers"' leads to a CLARIN-internal Trac instance.

p. 6: It is claimed that those and only those layer identifiers that are listed in the table under 2.2.2.1 can be used in FCS-QL queries. However, the example below (p. 7) contains identifiers "word" and "token" mot listed there (should have been "text"?), and the BNF grammar allows for any identifier.

p. 8-9: Links to the .xsd's lead to a CLARIN-internal Trac instance.

p. 8. It is said that all endpoints must implement the Generic Hits view as 'send-by-default'. No such thing is said about the Advanced view, but it is also designated as 'send-by-default'. Why is that?

p. 9: Advanced data view is 'send-by-default', but Advanced search should only be available for SRU 2.0 queries. So for 1.2, it shouldn't be available at all.

p. 9-10: In the advanced search results, how does a client understand which layer is which? They don't contain any unique identifiers such as 'pos' or 'word'. They only contain some arbitrary IDs (<Layer id="...">). By the way, I don't understand what those IDs are and where I get them from. Just write something random out of my head?

p. 10: "Send explain request without version and operation parameter" -- but an explain request has to have the operation parameter with 'explain' as its value, as is stated somewhere nearby. UPDATE: SRU documentation says that, indeed, an empty request is treated as an explain request. Maybe this exception is worth mentioning this explicitly here.

p. 12 and elsewhere: http://explain.z3950.org/dtd/2.0/ mentioned as the URL of the record schema, but its actual URL is http://zeerex.z3950.org/dtd/zeerex-2.0.dtd . Should I change that in the Explain response XML?

p. 12-13, example: what is "result-id" in SupportedLayer? What do I put as the text of the SupportedLayer element?

p. 13: It says 'scan' operation is not used for now, but CLARIN endpoint tester has tests that send scan requests. Why?

p. 14: x-cmd-resource-info parameter present in the query example, but never explained (mentioned in some 2013 slides on FCS; should now probably be x-fcs-endpoint-description)

p. 14: What is the sruResponse prefix? It is never mentioned. No namespace is provided here, so the example is actually invalid XML. (Who doesn't validate their examples before showing them to a wide audience -- in an official specification, no less??) Maybe sru was actually meant (see p. 2 and an SRU v. 1.2 example above)? I'm putting sru there for now.

p. 17: The BNF for FCS-QL makes it impossible to have multi-token queries where the first token has a quantifier, but is not parenthesized, e.g.:
[pos="NOUN"]{2,3} [text="dog"]
I guess this is a consequence of a sloppily written BNF grammar rather than an intended effect?

